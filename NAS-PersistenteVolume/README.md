First:
kubectl -n <your_namespace> apply -f odin-nfs.yaml

The server IP should be updated

Second:
kubectl -n <your_namespace> apply -f odin-storage.yaml

Third:
kubectl -n <your_namespace> apply -f odin-deploy.yaml