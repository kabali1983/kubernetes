kubectl label nodes pc1 servertype=lowmemory
kubectl label nodes pc2 servertype=lowmemory

Use after 03:
kubectl -n bind-server create secret generic regcred \
    --from-file=.dockerconfigjson=<path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson
