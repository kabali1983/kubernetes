kubectl apply -f namespace.yaml
kubectl apply -f production-tls-issuer.yaml
kubectl apply -f staging-tls-issuer.yaml
kubectl apply -f nginx-configmap.yaml
kubectl apply -f nginx-deployment.yaml
kubectl apply -f nginx-service.yaml
kubectl apply -f nginx-ingress-production.yaml

docker login https://dockerregistry.mvilla.org
kubectl -n docker-registry-server create secret generic regcred --from-file=.dockerconfigjson=/home/mvilla/.docker/config.json --type=kubernetes.io/dockerconfigjson



Marcos F. Villa: vas a tener que crear un nuevo htpasswd para un usuario
kubectl -n docker-registry-server exec -it nombre-del-pod -- sh
htpasswd -c /auth/htpasswd usuario
Marcos F. Villa: Entonces ahí te va a preguntar el password